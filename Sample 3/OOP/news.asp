<HTML>
<HEAD>
<TITLE>Newletter Signup</TITLE>
</HEAD>
<BODY>

<%
'Change the DBQ parameter to point to the location 
'of the db1.mdb database on your file system
Application("ConnectionString") = "DRIVER=Microsoft Access Driver (*.mdb);UID=admin;FIL=MS Access;DBQ=c:\WINDOWS\Desktop\Wrox\db1.mdb"

Dim bReturn

if Request.Form("Submit1") = "Submit" Then

	'Aquire late...
	Set objXML = Server.CreateObject("MSXML2.DOMDocument")
	
	'Create a blank "Root" XML document
	objXML.loadXML "<Root></Root>"
	Set root = objXML.documentElement
	
	'Create a "Name" child element and set it equal to our form variable
	'We should check to make sure that the value is not blank...
	'We could do this here, or better yet, by client side script
	Set objElement = objXML.createNode(1,"Name","")
	objElement.text = Request.Form("Name")
	Set currNode = root.insertBefore(objElement, root.childNodes.item(1))

	'Create an "Email" child element and set it equal to our form variable
	'Validation should also be done on Email. A great way is to check for an @ and .,
	'or by using regular expressions
	Set objElement = objXML.createNode(1,"Email","")
	objElement.text = Request.Form("Email")
	Set currNode = root.insertBefore(objElement, root.childNodes.item(1))
	
	'Release early...
	Set currNode = Nothing
	Set objElement = Nothing

	'Our classfactory object...
	'Aquire late...
	Set objSignup = Server.CreateObject("Registration.Classfactory")
	
	'Call the classfactory and tell it we need a Newsletter object
	bReturn = objSignup.DoRegistration(objXML.xml, "Registration.Newsletter", Application("ConnectionString"))
	
	'Release early...
	Set objSignup = Nothing
	Set objXML = Nothing
	
	'Everything was ok...
	'Of course, we could have also let the error trickle to the user...
	if bReturn then

	%>

	<P>Thank you for signing up!</P>

	<%
	
	'An error occurred
	else
	%>

	<P>This operation failed, please try again later</P>

	<%
	
	end if
	
else

%>

<P>
   <Form action="news.asp" method=post>
   Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
   <INPUT id=Name name=Name><br><br>
   Email Address:
   <INPUT id=Email name=Email><br><br>
   <INPUT id=submit1 style="LEFT: 317px; TOP: 15px" type=submit value=Submit name=submit1>
   </Form>
</P>
<%

end if 
%>
</BODY>
</HTML>
