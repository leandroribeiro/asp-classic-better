VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 1  'NoTransaction
END
Attribute VB_Name = "NewsLetter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Implements IRegister

Private Function IRegister_SignUp(strXML As String, Optional ByVal strConnection As String) As Boolean

On Error GoTo errHandler

    Dim con As ADODB.Connection
    Dim cmd As ADODB.Command
    
    Dim objXML As MSXML2.DOMDocument30
    
    Dim strSQL As String
    Dim lngReturnedRecs As Long
    
    Set objXML = New MSXML2.DOMDocument30
    
    objXML.loadXML strXML
    
    'Pass an XML specific error
    If objXML.parseError.errorCode <> 0 Then
        Err.Raise vbObjectError + 100, , "XML Parsing error"
    End If
    
    'Query to place the values in the database
    strSQL = "Insert into Newsletter (name, email) values (?,?)"
    
    'Acquire late...
    Set con = New ADODB.Connection
    
    With con
        .CursorLocation = adUseServer
        .Open strConnection
    End With

    'Acquire late...
    Set cmd = New ADODB.Command
    
    With cmd
        .ActiveConnection = con
        .CommandText = strSQL
        .CommandType = adCmdText
        .Parameters.Append .CreateParameter("Name", adVarChar, adParamInput, 50, objXML.getElementsByTagName("Name").Item(0).Text)
        .Parameters.Append .CreateParameter("Email", adVarChar, adParamInput, 100, objXML.getElementsByTagName("Email").Item(0).Text)
        .Execute lngReturnedRecs, , adExecuteNoRecords
    End With
    
    IRegister_SignUp = True
    
    'release asap...
    Set objXML = Nothing
    Set cmd = Nothing
    con.Close
    Set con = Nothing
    Exit Function
    
errHandler:

    'Check if this is an xml issue or something we should handle, if not...
    'Pass it up.
    IRegister_SignUp = False
    
    'we could uncomment this next line if we want the ASP page to
    'handle the exception
    'Err.Raise Err.Number, Err.Source, Err.Description
    
End Function
