VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Offerings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function SignupforNewsletter(ByVal sEmail As String)

    'Add them to the database

End Function

Public Function BuyGardeningTools(ByVal sFirstName As String, ByVal sLastName As String, ByVal sAddress As String, _
            ByVal sCity As String, ByVal sState As String, ByVal sZipCode As String, ByVal lCreditCardNumber As Long, _
            ByVal dtExpirationDate As Date)

    'Add them to the database
    'Charge their card
    'Send authorization to shipping department

End Function

Public Function SubscribetoMagazine(ByVal sFirstName As String, ByVal sLastName As String, ByVal sAddress As String, _
            ByVal sCity As String, ByVal sState As String, ByVal sZipCode As String, ByVal lCreditCardNumber As Long, _
            ByVal dtExpirationDate As Date)

    'Add them to the database
    'Charge their card
    'Send authorization to fulfillment center

End Function

Public Function PlaceanAd(ByVal sFirstName As String, ByVal sLastName As String, ByVal sAddress As String, _
            ByVal sCity As String, ByVal sState As String, ByVal sZipCode As String, ByVal sBillingAddress As String, _
            ByVal sCompanyName As String)

    'Add them to the database
    'Send email to billing department
    'Send Email to salesperson

End Function

Public Function SignupforSite(ByVal sFirstName As String, ByVal sLastName As String, ByVal sLoginID As String, _
            ByVal sPassword As String)
    
    'Add them to the database

End Function

Public Function GetJunesPromo(ByVal sFirstName As String, ByVal sLastName As String, ByVal sAddress As String, _
            ByVal sCity As String, ByVal sState As String, ByVal sZipCode As String, ByVal lCreditCardNumber As Long, _
            ByVal dtExpirationDate As Date, ByVal iItemNumber As Integer)

    'Add them to the database
    'Charge their card
    'Send authorization to shipping department
    'Send thank you email
    'Record statistic info

End Function

Public Function GetSeptPromo(ByVal sFirstName As String, ByVal sLastName As String, ByVal sAddress As String, _
            ByVal sCity As String, ByVal sState As String, ByVal sZipCode As String, ByVal lCreditCardNumber As Long, _
            ByVal dtExpirationDate As Date, ByVal iItemNumber As Integer)

    'Add them to the database
    'Charge their card
    'Send authorization to shipping department
    'Send thank you email
    'Record statistic info
    'Send info to VISA

End Function
