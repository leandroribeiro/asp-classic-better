<%
Dim objDatabase
Set objDatabase = Nothing
'-- Singleton for the CDatabase class
Function Database
If objDatabase Is Nothing Then
Set objDatabase = New CDatabase
End If
Set Database = objDatabase
End Function

'-- Begin CDatabase Class
Class CDatabase

	Private m_objConn

	Public Sub Class_Initialize()
		Set m_objConn = Nothing
	End Sub

	Public Property Get Connection

		'-- Reuse connection if one exist, create a new one otherwise
		If m_objConn Is Nothing Then

			Set m_objConn = Server.CreateObject("ADODB.Connection")

			'On Error Resume Next

			'm_objConn.Open Application.Contents.Item("ConnString")
			m_objConn.Open SQL_CONNECTION

			If Err.number <> 0 Then
				Set m_objConn = Nothing
				Response.Write "Error-->" & Err.description 'Application.Contents.Item("ErrorDatabaseConnect")
				Response.End
			End If

			On Error GoTo 0

		End If

		Set Connection = m_objConn

	End Property


	Public Function ExecuteQuery (SqlQuery)
		Dim objRs
		Set objRs = Server.CreateObject("ADODB.Recordset")
		objRs.Open SqlQuery, Me.Connection

		If Err.number <> 0 Then
			Set objRs = Nothing
			Response.Write "Error-->" & Err.description 'Application.Contents.Item("ErrorQueryExecute")
			Response.End
		End If

		On Error GoTo 0
		Set ExecuteQuery = objRs

	End Function

	Public Function ExecuteGetID (SqlQuery,tblName)
		Dim intRecords
		Dim objCmd
		intRecords = -1
		Set objCmd = Server.CreateObject("ADODB.Command")
		'On Error Resume Next
		objCmd.ActiveConnection = Me.Connection
		objCmd.CommandText = SqlQuery
		
		Call objCmd.Execute(intRecords)

		If Err.number <> 0 Then
			Set objCmd = Nothing
			Response.Write "Error-->" & Err.description 'Application.Contents.Item("ErrorQueryExecute")
			Response.End
		End If

		Set objRs = Server.CreateObject("ADODB.Recordset")
		Set objRs = Me.Connection.Execute("SELECT @@IDENTITY FROM " & tblName)
		ExecuteGetID = objRs.fields(0).value
		On Error GoTo 0

	End Function

	Public Function ExecuteCommand (SqlQuery)

		Dim intRecords
		Dim objCmd

		intRecords = -1

		'-- Create new command object
		Set objCmd = Server.CreateObject("ADODB.Command")

		On Error Resume Next

		objCmd.ActiveConnection = Me.Connection
		objCmd.CommandText = SqlQuery

		Call objCmd.Execute(intRecords)

		If Err.number <> 0 Then
		Set objCmd = Nothing

		Response.Write "Error-->" & Err.description 'Application.Contents.Item("ErrorQueryExecute")
		Response.End
		End If

		On Error GoTo 0

		ExecuteCommand = intRecords
	End Function

	Public Function CreateRecordset (SqlQuery, CursorType, LockType)

		Dim objRs

		'-- Create new recordset object
		Set objRs = Server.CreateObject("ADODB.Recordset")

		On Error Resume Next

		objRs.Open SqlQuery, Me.Connection, CursorType, LockType


		If Err.number <> 0 Then
		Set objRs = Nothing

		Response.Write "Error-->" & Err.description 'Application.Contents.Item("ErrorQueryExecute")
		Response.End
		End If

		On Error GoTo 0
		Set CreateRecordset = objRs

	End Function

	Public Function CreateRecordsetPage (SqlQuery, CursorType, LockType, iPageSize)

		Dim objRs

		'-- Create new recordset object
		Set objRs = Server.CreateObject("ADODB.Recordset")
		objRs.PageSize = iPageSize
		objRs.CacheSize = iPageSize


		On Error Resume Next

		objRs.Open SqlQuery, Me.Connection, CursorType, LockType


		If Err.number <> 0 Then
		Set objRs = Nothing

		Response.Write "Error-->" & Err.description 'Application.Contents.Item("ErrorQueryExecute")
		Response.End
		End If

		On Error GoTo 0
		Set CreateRecordsetPage = objRs
		End Function
		Public Sub Class_Terminate()
		Set m_objConn = Nothing

	End Sub

End Class
%>