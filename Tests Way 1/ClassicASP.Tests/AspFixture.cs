﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using NUnit.Framework;

namespace ClassicASP.Tests
{
    public abstract class AspFixture
    {
        private const string BASE_URI = "http://localhost/asp3here/";

        public void RunAspTest(string uri)
        {
            var request = WebRequest.Create(BASE_URI + uri);

            ICredentials requestCredentials = CredentialCache.DefaultCredentials;
            request.Credentials = requestCredentials;

            using (var response = request.GetResponse())
            {
                using (var stream = new StreamReader(response.GetResponseStream()))
                {
                    string html = stream.ReadToEnd();

                    Assert.IsFalse(html.Contains("Failed"));
                }
            }
        }
    }
}
