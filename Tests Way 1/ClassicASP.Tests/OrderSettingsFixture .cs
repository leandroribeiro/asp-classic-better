﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace ClassicASP.Tests
{
    [TestFixture]
    public class OrderSettingsFixture : AspFixture
    {
        [Test]
        public void OrderSettingsTest()
        {
            RunAspTest("OrderSettingsTest.asp");
        }
    }
}
