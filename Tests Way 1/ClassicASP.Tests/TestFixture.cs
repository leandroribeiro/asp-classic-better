﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace ClassicASP.Tests
{
    [TestFixture]
    public class TestFixture : AspFixture
    {
        [Test]
        public void Test()
        {
            RunAspTest("test.asp");
        }
    }
}
