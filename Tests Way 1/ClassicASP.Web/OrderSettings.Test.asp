﻿<!--#include virtual="asp3here/UnitTester.asp"-->
<!--#include virtual="asp3here/OrderSettings.asp"-->
<%
dim tester
Set tester = new UnitTester
tester.Init("Testing Order Settings")

Dim currentOrderSettings
Set currentOrderSettings = new OrderSettings

currentOrderSettings.CutOffTime = "01/01/1999 13:00:00"
currentOrderSettings.NextOrderTime = "01/01/1999 14:00:00"

Dim onePm
onePm = "10/10/2008 13:00:00"

tester.AssertAreEqual currentOrderSettings.CanOrderAt(onePm), True
'tester.AssertIsFalse currentOrderSettings.CanOrderAt(onePm), "Can Not Order At 13:00"

%>