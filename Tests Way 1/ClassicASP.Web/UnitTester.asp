﻿<%
Class UnitTester
   Public Sub Init(testMessage)

      Response.Write("<style type='text/css'> .unitTest {color: #CF118C;}")
      Response.Write(" .testMessage { padding-right:50px }")
      Response.Write(" .passed { color:green; font-weight:bold; }")
      Response.Write(" .failure { color:red; font-weight:bold; }")
      Response.Write(" .unitTest, .test, .given { font-family:Verdana; font-size:14px; }")
      Response.Write(" .given,  { font-weight:bold; }")
      Response.Write("</style>")

      Response.Write("<div class='unitTest'><h1>Unit testing for ASP</h1>")
      Response.Write("<h2>" + testMessage + "</h2></div>")

   End Sub

    Public Sub Given(message)
   Response.Write("<hr><div class='given'> Given " + message + "</div>")
End Sub

Private Sub WriteTest(passed, message)
   Response.Write("<br><div class='test'>Test: ")

   If passed Then
       Response.Write("<span class='passed'>Passed</span>")
   Else
       Response.Write("<span class='failure'>Failed</span>")
   End If

   Response.Write("<span class='testMessage'> - " + message + "</span></div>")   
End Sub

Public Function AssertAreEqual(actual, expected)
   Dim areEqual, sActual, sExpected
   areEqual = (actual=expected)

   'dim sActual, sExpected
   If IsNull(actual) Then
       sActual = "NULL"
   Else
       sActual = CStr(actual)
   End If

   If IsNull(expected) Then
       sExpected = "NULL"
   Else
       sExpected = CStr(expected)
   End If

   WriteTest areEqual, ("Actual value of <b>" + sActual + "</b> is expected to be <b>" _
                          + sExpected + "</b>")
   AssertAreEqual = areEqual
End Function


'Public Function AssertIsFalse(actual, expected)
'   Dim areEqual, sActual, sExpected
'   areEqual = (actual=expected)
'
'   'dim sActual, sExpected
'   If IsNull(actual) Then
'       sActual = "NULL"
'   Else
'       sActual = CStr(actual)
'   End If
'
'   If IsNull(expected) Then
'       sExpected = "NULL"
'   Else
'       sExpected = CStr(expected)
'   End If
'
'   WriteTest areEqual, ("Actual value of <b>" + sActual + "</b> is expected to be <b>" _
'                          + sExpected + "</b>")
'   AssertAreEqual = areEqual
'End Function


End Class
%>