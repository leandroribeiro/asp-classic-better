<%

'REFERENCIA'
'http://imasters.com.br/artigo/4414/asp/banco_de_dados_com_orientacao_a_objetos_em_asp/'

Class clientes_dao

    Private m_lista
    Private M_StartPage
    Private M_StartRow
    Private M_MaxRows
    Private M_RecordCount
    Private M_OrderBy

    Private m_conn

	Public Property Get StartPage()
	Startpage = M_Startpage
	end Property

	Public Property Let Startpage(p_data)
	M_Startpage = p_data
	end Property

	Public Property Get OrderBy()
	OrderBy = M_Orderby
	end Property

	Public Property Let OrderBy(p_data)
		M_OrderBy = p_data
	end Property

	Public Property Get RecordCount()
		RecordCount = M_RecordCount
	end Property

	Public Property Let RecordCount(p_data)
    End Property

	Public Property Get MaxRows()
		MaxRows = M_MaxRows
	end Property

	Public Property Let MaxRows(p_data)
		M_MaxRows = p_data
	end Property

    Public Sub Class_Initialize()
        M_StartRow = 0
        M_MaxRows = 0
        Set m_lista = Server.CreateObject("Scripting.Dictionary")
    End Sub

    Public Sub Class_Terminate()
        m_conn = Nothing
        m_lista = Nothing
    End Sub

    Public Function Initialize(ByRef conn)
        Set m_conn = conn
    End Function

    Public Function Insert(obj)

        Set rs = Server.CreateObject("ADODB.Recordset")
        rs.CursorLocation = 3
        rs.Open "select * from clientes where 1=0 ", m_conn, 1, 2
        ArrFlds = array("nome")
        ArrValues = array(obj.nome)
        rs.AddNew ArrFlds, ArrValues
        rs.Update()
        Insert = rs(0)
        rs.Close()
        'rs = Nothing
    End Function

    Public Function Update(obj)
        If Not isnumeric(obj.id) Or isEmpty(obj.id) Then
            U_id = 0
        Else
            U_id = obj.id
        End If
        If Not trim(obj.nome) = "" Then
            U_nome = SingleQuotes(obj.nome)
        Else
            U_nome = ""
        End If
        strSQL = strSQL & " UPDATE clientes SET "
		strSQL = Strsql& " nome ='"&U_nome&"'"
		strSQL = strSQL & " where id= " &obj.id
        chave = obj.id
        If isnumeric(chave) And (Not isempty(chave)) And chave <> 0 Then
            m_conn.execute(strsql)
        End If
    End Function

    Function datainv(d)
        If isdate(d) Then datainv = year(d) & "-" & strzero(month(d), 2) & "-" & strzero(day(d), 2)
    End Function

    Private Function strzero(v, z)
		strzero=string(z-len(trim(cstr(v))),"0")&trim(cstr(v))
    End Function

    Public Function Delete(key)
        If isnumeric(key) And (Not isempty(key)) And key <> 0 Then
            m_conn.execute("delete from clientes where id=" & key)
        End If
    End Function

    Public Function FindAll() 'Objeto Scriting Dictinary
        Set rs = Server.CreateObject("ADODB.Recordset")
        rs.CursorLocation = 3
        rs.Open "select * from clientes " & m_orderby, m_conn, 1, 3
        FillFromRS(rs)
        rs.close()
        Set rs = Nothing
        Set findall = m_lista
    End Function

    Public Function Findbywhere(pfilter) 'Objeto Scriting Dictinary
        pfilter = SingleQuotes(pfilter)
        Set rs = Server.CreateObject("ADODB.Recordset")
        rs.CursorLocation = 3
		rs.Open "select * from clientes where"&pfilter&" "&m_orderby,m_conn, 1, 3
        FillFromRS(rs)
        rs.close()
        rs = Nothing
        findbywhere = m_lista
    End Function

    Public Function FindByid(pfilter) 'Objeto Scriting Dictinary
        If Not isnumeric(pfilter) Or isEmpty(pfilter) Then
            pfilter = 0
        Else
            pfilter = CLng(pfilter)
        End If
        Set rs = Server.CreateObject("ADODB.Recordset")
        rs.CursorLocation = 3
		rs.Open "select * from clientes where id="&pfilter&" "&m_orderby,m_conn, 1, 3
        FillFromRS(rs)
        rs.close()
        rs = Nothing
        FindByid = m_lista
    End Function

    Public Function FindBynome(pfilter) 'Objeto Scriting Dictinary
        If Not trim(pfilter) = "" Or isEmpty(pfilter) Then
            pfilter = ""
        Else
            pfilter = SingleQuotes(CStr(pfilter))
        End If
        Set rs = Server.CreateObject("ADODB.Recordset")
        rs.CursorLocation = 3
		rs.Open "select * from clientes where nome like '"&pfilter&"'' "&m_orderby,m_conn, 1, 3
        FillFromRS(rs)
        rs.close()
        rs = Nothing
        FindBynome = m_lista
    End Function

    Private Function SingleQuotes(pStringIn)
        If pStringIn = "" Or isnull(pStringIn) Then Exit Function
        Dim pStringModified
		pStringModified = Replace(pStringIn,"’","”")
        pStringModified = Replace(pstringmodified, "\", "\\")
        SingleQuotes = pStringModified
    End Function

    Private Function FillFromRS(p_RS)
        Dim myBook
        m_recordcount = p_rs.recordcount
        If m_recordcount > 0 Then
            If m_maxrows <> 0 And m_startpage <> 0 Then
                p_Rs.PageSize = m_maxrows
                p_rs.AbsolutePage = m_startpage
                m_lista = Nothing
                Set m_lista = Server.CreateObject("Scripting.Dictionary")
                Rc = 0
                Do While ((Not p_Rs.eof) And (rc < p_Rs.PageSize))
                    mybook = New DTO_clientes
                    MyBook.id = p_RS.fields("id").Value
                    MyBook.nome = p_RS.fields("nome").Value
                    m_lista.Add myBook.id, myBook
                    Rc = Rc + 1
                    p_RS.movenext()
                Loop
            Else
                Set m_lista = Nothing
                Set m_lista = Server.CreateObject("Scripting.Dictionary")
                Do While ((Not p_Rs.eof))
                    Set mybook = New DTO_clientes
                    MyBook.id = p_RS.fields("id").Value
                    MyBook.nome = p_RS.fields("nome").Value
                    m_lista.Add myBook.id, myBook
                    Rc = Rc + 1
                    p_RS.movenext()
                Loop
            End If
        Else
            Set m_lista = Server.CreateObject("Scripting.Dictionary")
        End If
    End Function

End Class
%>